import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import xlrd

FILE_NAME = 'fire_theft.xls'

#reading the data:

book = xlrd.open_workbook(FILE_NAME, encoding_override="utf-8")
sheet = book.sheet_by_index(0)
data = np.asarray([sheet.row_values(i) for i in range(1, sheet.nrows)])

#create placeholders for input X and Y:

X = tf.placeholder(tf.float32, name='X')
Y = tf.placeholder(tf.float32, name='Y')

#create weight and bias:
w = tf.Variable(0.0, name='Weights')
b = tf.Variable(0.0, name='Bias')

#model to predict Y:
Y_predicted = X * w + b

#square the error to calculate the loss:
loss = tf.square(Y - Y_predicted, name='Loss')

#train to minimize the loss:
optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.001).minimize(loss)

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    
    #train the model 100 times
    for i in range(100):
        total_loss = 0
        for x, y in data:
            _, l = sess.run([optimizer, loss], feed_dict={X: x, Y: y})
            total_loss += l
        print ('Epoch {0}: {1}'.format(i, total_loss / sheet.nrows))
            
    #output weight and bias:
    w_value, b_value = sess.run([w, b])
    
#display the results:
X, Y = data.T[0], data.T[1]
plt.plot(X, Y, 'bo', label='Real data')
plt.plot(X, X*w_value+b_value, 'r', label='Predicted data')
plt.legend()
plt.show()
